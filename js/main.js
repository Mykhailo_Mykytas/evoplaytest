/**
 * Created by M on 18.07.17.
 */
$(document).ready(function () {
    var data = [
        {
            "win": 2500100,
            "id": "cj2um0d1jvxgy0103mh4yqt2f",
            "game": ["cj2rnmvo4txlp0146ex2dtl4f"],
            "userName": "mayWinner",
            "month": "may"
        }, {
            "win": 100200,
            "id": "cj2vo7cs8lk7b0128nqmqasn2",
            "game": ["cj2rnm70nttog0146645wumcc"],
            "userName": "juneWinner",
            "month": "june"
        }, {
            "win": 1400700,
            "id": "cj2vo7zpel59p01031zss0jts",
            "game": ["cj2rnlk1wtro80146so47gjva"],
            "userName": "aprilWinner",
            "month": "april"
        }, {
            "win": 5100250,
            "id": "cj2vo8p6gl7bi01038eptf8e3",
            "game": ["cj3a9z5a0eh4c01576jksq4wm"],
            "userName": "may_mirtrud",
            "month": "may"
        }, {
            "win": 900750,
            "id": "cj2vo9ax2l7h30103mh0qywfb",
            "game": ["cj2rnmvo4txlp0146ex2dtl4f"],
            "userName": "pervomay",
            "month": "may"
        }, {
            "win": 600700,
            "id": "cj2vo9rq5l7mm010381ite7c5",
            "game": ["cj2rnlk1wtro80146so47gjva"],
            "userName": "userMay_888",
            "month": "may"
        }, {
            "win": 50780,
            "id": "cj2voa7s4lp460128vmk3t2ar",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "prerevertenMay",
            "month": "may"
        }, {
            "win": 300100,
            "id": "cj2voav3dlqjk0128ra5evq0q",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "KotMartovskyi",
            "month": "march"
        }, {
            "win": 80500,
            "id": "cj2vobdhclaj90103v7dytkzb",
            "game": ["cj3aa4lgoekwm0157441lvgyd"],
            "userName": "jaMayka",
            "month": "may"
        }, {
            "win": 35050,
            "id": "cj2vobw67lqyn0128rwcfpjsm",
            "game": ["cj2rnmvo4txlp0146ex2dtl4f"],
            "userName": "laMajka",
            "month": "may"
        }, {
            "win": 6900450,
            "id": "cj2vocooalbvg0103wmnw13kd",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "pervoApril",
            "month": "april"
        }, {
            "win": 347,
            "id": "cj2vocooalbvg0103wmnw13kd",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "julyPretendent",
            "month": "july"
        }, {
            "win": 100000000000,
            "id": "cj2vocooalbvg0103wmnw13kd",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "julyPretendent",
            "month": "july"
        }, {
            "win": 100000000000,
            "id": "cj2vocooalbvg0103wmnw13kd",
            "game": ["cj2rnl6fftnpx014624x5rie6"],
            "userName": "julyPretendent",
            "month": "november"
        }
    ];

    processData()
    function processData() {
        var sortedData = sortByMonth(data);
        getMonthsWithWins(sortedData);
        getTopWinners(sortedData);
    }


    function sortByMonth(arr) {
        var months = ["january", "february", "march", "april", "may", "june",
            "july", "august", "september", "october", "november", "december"];
        var curMonth = new Date().getMonth()
        curMonth = months[curMonth];
        for (var m = 0; ;){
            if(months[months.length-1] != curMonth){
                months.push(months[0])
                months.shift()
            }else{
                break
            }
        }
        console.log(months)
        arr.sort(function (a, b) {
            if(months.indexOf(a.month) < months.indexOf(b.month)) return 1;
            if(months.indexOf(a.month) > months.indexOf(b.month)) return -1;
           /* if(keyA > keyB) return 1;
            return months.indexOf(a.month)
                - months.indexOf(b.month);*/
        });
        console.log(arr)
        return arr;


    }
    function getMonthsWithWins(sortedData){
        var winMonths = [];
        for(var n = 0; n < sortedData.length; n++){
            if(winMonths.indexOf(sortedData[n].month) == -1){
                winMonths.push(sortedData[n].month);
            }
        }
        console.log(winMonths)
        drawWinMonths(winMonths);
        return winMonths.length;
    }
    function drawWinMonths(months) {
        $('select').html('<option disabled>Выберите месяц</option>');
        for(var m = 0; m < months.length; m++){
            $('select').append('<option value="'+months[m]+'">'+months[m]+'</option>')
        }
    }
    function getTopWinners(sortedData) {
        var topWinners = [];
         for (var w = 0; w < sortedData.length; w++){
             if(topWinners.length == 0){
                 topWinners.push({month: sortedData[w].month, name:sortedData[w].userName, win: sortedData[w].win});
             }else{
                 for(var tw = 0; tw < topWinners.length; tw++){
                     if(sortedData[w].win > topWinners[tw].win && sortedData[w].month == topWinners[tw].month){
                        topWinners[tw].name = sortedData[w].userName;
                        topWinners[tw].win = sortedData[w].win;
                     }else{
                         if(!isIn()){
                             topWinners.push({month: sortedData[w].month, name:sortedData[w].userName, win: sortedData[w].win});
                         }
                        function isIn() {
                            var found = false;
                            for(var i = 0; i < topWinners.length; i++) {
                                if (topWinners[i].month == sortedData[w].month) {
                                    found = true;
                                    break;
                                }
                            }
                            return found;
                        }
                     }
                 }
             }
         }
        drawTopWinnners(topWinners);
    }
    function drawTopWinnners(topWinners) {
        $('.top-list').html('')
        for(var tw = 0; tw < topWinners.length; tw++){
            $('.top-list').append('<li>Month: '+topWinners[tw].month+'; User:  '+topWinners[tw].name+'; Price:  '+topWinners[tw].win+'$</li>')
        }
    }
});